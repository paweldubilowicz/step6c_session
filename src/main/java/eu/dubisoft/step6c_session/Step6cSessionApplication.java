package eu.dubisoft.step6c_session;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling // INFO: anotacja potrzebna zeby schedulery wiedzialy ze maja dzialac
public class Step6cSessionApplication {

  public static void main(String[] args) {
    SpringApplication.run(Step6cSessionApplication.class, args);
  }

}
