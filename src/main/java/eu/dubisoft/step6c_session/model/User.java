package eu.dubisoft.step6c_session.model;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {

  public User(String userId, String login, String password, List<Permission> permissions) {
    this.userId = userId;
    this.login = login;
    this.password = password;
    this.permissions = permissions;
  }

  private String userId;
  private String login;
  private String password;
  private List<Permission> permissions;
}
