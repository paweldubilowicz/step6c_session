package eu.dubisoft.step6c_session.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Session {

  // INFO: ten model powinien zawierac wszystkie informacje, ktore sa potrzebne w procesie autoryzacji

  public Session(String userId, String login, String token, List<Permission> permissions, boolean isAnonymous) {
    this.userId = userId;
    this.login = login;
    this.token = token;
    this.permissions = permissions;
    this.isAnonymous = isAnonymous;
  }

  private String userId;
  private String login;
  private String token;
  private List<Permission> permissions;
  private boolean isAnonymous; // INFO: tutaj trzymamy informacje czy uzytkownik jest zautentykowany
  private Date expirationDate; // INFO: do kiedy sesja jest wazna

  // INFO: Mozna tez w tej klasie osadzic cos co zachowaniem bedzie przypominac session store i bedzie przechowywac dane dodatkowe,
  // do ktorych chcemy miec latwy i szybki dostep miedzy requestami (na przyklad statystyki dzialania uzytkownika w obrebie sesji)
  // Trzeba jednak miec na uwadze 2 rzeczy:
  // - te dane beda zajmowac dodatkowa pamiec - wiec przy duzej liczbie uzytkownikow moze sie to okazac zabojcze
  // - ten mechanizm czyni system STANOWYM, co oznacza, ze zastosowanie architektury mikroserwisowej zostaje poteznie utrudnione
  // W tym przykladzie logujemy liste serwisow, ktore zostaly wywolane
  private List<String> executedServices = new ArrayList<>();

  public boolean isTokenExpired() {
    return new Date().after(this.expirationDate);
  }

  public void resetExpirationDate(int sessionTimeout) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.SECOND, sessionTimeout);
    this.expirationDate = calendar.getTime();
  }
}
