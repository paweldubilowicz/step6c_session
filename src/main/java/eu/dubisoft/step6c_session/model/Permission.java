package eu.dubisoft.step6c_session.model;

public enum Permission {

  ADMIN,
  USER,
  FULL_ACCESS;
}
