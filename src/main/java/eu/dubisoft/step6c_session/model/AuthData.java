package eu.dubisoft.step6c_session.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthData {

  private String login;
  private String password;
}
