package eu.dubisoft.step6c_session.api;

import eu.dubisoft.step6c_session.config.utils.SessionStore;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

  @GetMapping({"/adminService"})
  public String adminService() {
    SessionStore.logServiceUsage("adminService");
    return "adminService success";
  }

  @GetMapping({"/userService"})
  public String userService() {
    SessionStore.logServiceUsage("userService");
    return "userService success";
  }

  @GetMapping({"/commonService"})
  public String commonService() {
    SessionStore.logServiceUsage("commonService");
    return "commonService success";
  }

  @GetMapping({"/authenticatedService"})
  public String authenticatedService() {
    SessionStore.logServiceUsage("authenticatedService");
    return "authenticatedService success";
  }

  @GetMapping({"/openService"})
  public String openService() {
    SessionStore.logServiceUsage("openService");
    return "openService success";
  }

  @GetMapping({"/unexpectedService"})
  public String unexpectedService() {
    SessionStore.logServiceUsage("unexpectedService");
    return "unexpectedService success";
  }

  @GetMapping({"/executedServiceList"})
  public List<String> executedServiceList() {
    return SessionStore.getExecutedServices();
  }
}