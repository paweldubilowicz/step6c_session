package eu.dubisoft.step6c_session.api;

import eu.dubisoft.step6c_session.model.AuthData;
import eu.dubisoft.step6c_session.model.TokenResponse;
import eu.dubisoft.step6c_session.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

  private final AuthService authService;

  @PostMapping("/login")
  public TokenResponse login(@RequestBody AuthData authData) {
    return authService.loginAndGenerateToken(authData);
  }

  @GetMapping("/logout")
  public void logout(@RequestHeader("X_AUTH_TOKEN") String token) { // INFO: mozemy pobrac wartosci konkretnych headerow w taki sposob
    authService.logout(token);
  }
}
