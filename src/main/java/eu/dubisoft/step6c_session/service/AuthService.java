package eu.dubisoft.step6c_session.service;

import eu.dubisoft.step6c_session.config.utils.RandomUtils;
import eu.dubisoft.step6c_session.exceptions.InvalidCredentialsException;
import eu.dubisoft.step6c_session.model.AuthData;
import eu.dubisoft.step6c_session.model.Session;
import eu.dubisoft.step6c_session.model.TokenResponse;
import eu.dubisoft.step6c_session.model.User;
import eu.dubisoft.step6c_session.repository.SessionMemoryRepository;
import eu.dubisoft.step6c_session.repository.UserMemoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

  @Value("${session.one_token_per_user}")
  private boolean oneSessionPerUser;
  @Value("${session.timeout}")
  private int sessionTimeout;

  private final SessionMemoryRepository sessionRepository;
  private final UserMemoryRepository userRepository;

  public Session findSession(String token) {
    return sessionRepository.findByToken(token);
  }

  // INFO: jedna potezna zmiana w tym przykladzie to sposob autentykacji i przechowywania sesji, nie uzywamy w tym celu zadnych mechanizmow wewnetrznych
  // Sami odpowiadamy za:
  // - walidacje danych wejsciowych, znalezienie wlasciwego uzytkownika i weryfikacje hasla
  // - utworzenie (i w tym przypadku ewentualne nadpisanie) sesji
  // - utrzymywanie i usuwanie sesji
  public TokenResponse loginAndGenerateToken(AuthData authData) {
    if (authData == null) {
      throw new InvalidCredentialsException();
    }
    User user = userRepository.findByLoginAndPassword(authData.getLogin(), authData.getPassword());

    if (oneSessionPerUser) {
      // INFO: tutaj usuwamy istniejaca sesje uzytkownika, jezeli jest taka potrzeba
      Session existingSession = sessionRepository.findByToken(user.getUserId());
      if (existingSession != null) {
        sessionRepository.deleteByToken(existingSession.getToken());
      }
    }

    String token = RandomUtils.generateToken();
    while (sessionRepository.isTokenInUse(token)) {
      token = RandomUtils.generateToken();
    }
    Session newSession = new Session(user.getUserId(), user.getLogin(), RandomUtils.generateToken(), user.getPermissions(), false);
    newSession.resetExpirationDate(sessionTimeout);
    sessionRepository.saveSession(newSession);
    return new TokenResponse(newSession.getToken());
  }

  public void logout(String token) {
    // INFO: alternatywnie moglibysmy nie przekazywac headera z zewnatrz, lecz pobrac go z SecurityUtil
    sessionRepository.deleteByToken(token);
  }
}
