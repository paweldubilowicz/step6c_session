package eu.dubisoft.step6c_session.config;

import eu.dubisoft.step6c_session.config.filters.AuthorizationFilter;
import eu.dubisoft.step6c_session.config.filters.ExceptionHandlerFilter;
import eu.dubisoft.step6c_session.config.filters.SimpleCorsFilter;
import eu.dubisoft.step6c_session.config.handlers.RestAccessDeniedHandler;
import eu.dubisoft.step6c_session.model.Permission;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final RestAccessDeniedHandler accessDeniedHandler;
  private final AuthorizationFilter authorizationFilter;
  private final SimpleCorsFilter simpleCorsFilter;
  private final ExceptionHandlerFilter exceptionHandlerFilter;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER).and()

        .headers()
          .cacheControl().and()
          .frameOptions().deny()
          .xssProtection().xssProtectionEnabled(true).and()
          .contentTypeOptions().and()
        .and()
        .csrf().disable()

        .addFilterBefore(authorizationFilter, BasicAuthenticationFilter.class)
        .addFilterBefore(simpleCorsFilter, AuthorizationFilter.class)
        .addFilterBefore(exceptionHandlerFilter, SimpleCorsFilter.class)

        .authorizeRequests()
        .antMatchers("/test/adminService").hasAnyAuthority(Permission.ADMIN.name(), Permission.FULL_ACCESS.name())
        .antMatchers("/test/userService").hasAnyAuthority(Permission.USER.name(), Permission.FULL_ACCESS.name())
        .antMatchers("/test/commonService").hasAnyAuthority(Permission.USER.name(), Permission.ADMIN.name(), Permission.FULL_ACCESS.name())
        .antMatchers("/test/authenticatedService").authenticated()
        .antMatchers("/auth/logout").authenticated()

        .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler)
    ;
  }
}
