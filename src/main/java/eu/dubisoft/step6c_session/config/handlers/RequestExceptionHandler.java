package eu.dubisoft.step6c_session.config.handlers;

import eu.dubisoft.step6c_session.exceptions.InternalException;
import eu.dubisoft.step6c_session.exceptions.InvalidCredentialsException;
import eu.dubisoft.step6c_session.exceptions.SessionExpiredException;
import eu.dubisoft.step6c_session.exceptions.SessionNotFoundException;
import eu.dubisoft.step6c_session.model.ErrorResponse;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class RequestExceptionHandler {

    @ExceptionHandler(InvalidCredentialsException.class)
    public ResponseEntity<ErrorResponse> handle(final InvalidCredentialsException e) {
        return new ResponseEntity(new ErrorResponse(400, "INVALID_CREDENTIALS", "Invalid login and/or password", new Date()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handle(final IllegalArgumentException e) {
        return new ResponseEntity(new ErrorResponse(400, "INVALID_INPUT", e.getMessage(), new Date()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SessionNotFoundException.class)
    public ResponseEntity<ErrorResponse> handle(final SessionNotFoundException e) {
        return new ResponseEntity(new ErrorResponse(401, "UNAUTHORIZED", "Session not found", new Date()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(SessionExpiredException.class)
    public ResponseEntity<ErrorResponse> handle(final SessionExpiredException e) {
        return new ResponseEntity(new ErrorResponse(401, "UNAUTHORIZED", "Session expired", new Date()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(InternalException.class)
    public ResponseEntity<ErrorResponse> handle(final InternalException e) {
        log.error("Internal error", e);
        return new ResponseEntity(new ErrorResponse(500, "SERVER_ERROR", e.getMessage(), new Date()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handle(final Exception e) {
        log.error("Unknown error", e);
        return new ResponseEntity(new ErrorResponse(500, "UNKNOWN_ERROR", "Unknown error", new Date()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
