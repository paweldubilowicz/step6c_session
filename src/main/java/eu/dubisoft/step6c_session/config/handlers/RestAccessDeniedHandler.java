package eu.dubisoft.step6c_session.config.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dubisoft.step6c_session.model.ErrorResponse;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RestAccessDeniedHandler implements AccessDeniedHandler {

    private final ObjectMapper objectMapper;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception) throws IOException, ServletException {
        response.getWriter().append(objectMapper.writeValueAsString(new ErrorResponse(403, "ACCESS_DENIED", exception.getMessage(), new Date())));
        response.setStatus(403);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    }
}