package eu.dubisoft.step6c_session.config.filters;

import eu.dubisoft.step6c_session.config.constants.Headers;
import eu.dubisoft.step6c_session.exceptions.SessionExpiredException;
import eu.dubisoft.step6c_session.exceptions.SessionNotFoundException;
import eu.dubisoft.step6c_session.model.Session;
import eu.dubisoft.step6c_session.service.AuthService;
import java.io.IOException;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
public class AuthorizationFilter extends OncePerRequestFilter {

  @Value("${session.timeout}")
  private int sessionTimeout;
  @Value("${session.refresh_on_activity}")
  private boolean sessionRefreshOnActivity;

  private final AuthService authService;

  // INFO: tutaj robimy nieco inne podejscie do serwisow otwartych, mianowicie nie robimy wykluczenia z filtracji dla tych serwisow.
  // Czasami mozemy chiec miec informacje o zalogowanym uzytkowniku uzywajacym otwartego zasobu.

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
    // INFO: w komunikacji RESTowej funkcjonuje cos takiego jak preflight check, np. przed POSTem wysylany jest najpierw request typu OPTIONS,
    // request taki nie powinien podlegac walidacji tokenowej
    return "OPTIONS".equalsIgnoreCase(request.getMethod());
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

    String token = request.getHeader(Headers.X_AUTH_TOKEN.name()); // INFO: zawsze lepiej trzymac gdzie constanta
    if (token == null || token.trim().length() == 0) {
      // INFO: Jezeli nie podajemy tokena, to prawdopodobnie chcemy sie dobic do otwartego zasobu - nie jest to bledem.
      // Zwrocmy tutaj uwage na fakt, ze zostawiamy logike dalszego zachowania Springowi.
      // Trzeba pamietac, ze filtr powinien zawsze wywolywac kolejny filtr w lancuchu, a nie po prostu przerywac (poza wystapieniem wyjatkow)
      chain.doFilter(request, response);
      return;
    }
    Session session = authService.findSession(token);
    if (session == null) {
      // INFO: Brak tokena i brak sesji to rozne rzeczy - dlatego tez trzeba je inaczej obsluzyc
      // Bez tokena mozemy uderzac tylko na niezabezpieczone serwisy, ktore tokena nie wymagaja,
      // ale jezeli przy ich wywolaniu dostana wazny token - tez nic sie nie stanie - po prostu bedziemy wiedziec kto uderza na otwarty serwis
      // Jezeli jednak probujemy uderzyc do dowolnego serwisu (nawet niezabezpieczonego) z niewaznym tokenem - cos jest nie tak, moze to byc atak, albo token moze wymagac odswiezenia
      throw new SessionNotFoundException(); // INFO: Rzucamy wlasny customowy wyjatek, ktory jest przechwytywany w RequestExceptionHandler
    }
    if (session.isTokenExpired()) {
      throw new SessionExpiredException();
    }

    if (sessionRefreshOnActivity) {
      // INFO: tutaj dorzucam mechanizm, ktory czesto mozna zobaczyc w bankach - sesja wygasa po jakims okresie bez aktywnosci
      // Czyli kazda aktywnosc resetuje czas zycia sesji
      session.resetExpirationDate(sessionTimeout);
    }

    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
        session,
        null,
        session.getPermissions()
            .stream()
            .map(it -> new SimpleGrantedAuthority(it.name()))
            .collect(Collectors.toList()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    chain.doFilter(request, response);
  }
}
