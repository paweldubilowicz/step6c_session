package eu.dubisoft.step6c_session.config.utils;

import java.util.UUID;

public class RandomUtils {

  public static String generateToken() {
    return UUID.randomUUID().toString();
  }
}
