package eu.dubisoft.step6c_session.config.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HashingUtils {

  // INFO
  // standardowe narzedzie do szyfrowania hasel, dostepnych jest kilka szyfrow, konfiguracja charseta... takie tam
  // pole salt - tutaj sie dzieje kawalek magii, dla kazdego systemu wstukuje sie tu randomowy string (hardkodujemy go)

  // opowiem tutaj o lamaniu hasel przez rainbow tables
  // zalozmy ze jedyne co robimy z haslem to szyfrujemy je na przyklad SHA-512
  // wowczas jakis haxor sobie szuka na necie istniejacych rainbow tables dla SHA-512, wstukuje przechwycony hash hasla i potencjalnie znajduje haslo
  // czyli hax polega na tym ze rainbow table ma juz w sobie wynik dla hasha X rowny np. haslo123

  // zabezpieczamy sie wiec, w naszym systemie do hasla na przodzie (albo na tyle, albo po obu stronach) dodajemy salt
  // tutaj h4x musialby polegac ze w rainbow table jest wynik dla hasha X rowny j8(h^cE61Sh$00Vahaslo123
  // nawet jezeli haxy przechwycily by salta to musieliby budowac rainbow table specyficznie pod system zeby to mialo sens, wiec od razu jest bezpieczniej

  // ja w tym narzedziu poszedlem krok dalej, bo uzywany salt jest zalezny od loginu (login + salt)
  // w efekcie trzeba by bylo budowac tabele pod konkretnego uzytkownika - wymagane zasoby sa potezne, a dostep tylko do 1 konta

  // jedno zastrzezenie - salt nie moze sie zmieniac (bo wszyscy by musieli zmienic swoje hasla), a zmiana loginu moze byc tylko razem ze zmiana hasla

  // tez wspomne o bezpieczenstwie - w bazie nie jest przechowywane haslo, tylko jego hash,
  // plaintextowe haslo pojawia sie:
  // - na froncie w formularzu logowania/zakladania konta/resetu hasla
  // - w transmisji z frontu na backend - dlatego wymagany jest SSL
  // - przy odbieraniu na backendzie - gdzie jest szyfrowane i zapominane

  private final static String CHARSET_NAME = "UTF-8";
  private final static String digest = "SHA-512";
  private final static String salt = "j8(h^cE61Sh$00Va";

  public static void main(String[] args) {
    log.info(HashingUtils.hashPassword("admin", "dupa.8"));
  }

  public static String hashPassword(String username, String password) {
    if (password == null) {
      return null;
    }
    try {
      MessageDigest md = MessageDigest.getInstance(digest);
      md.update(("" + username + salt).getBytes(CHARSET_NAME));
      byte[] bytes = md.digest(password.getBytes(CHARSET_NAME));
      StringBuilder sb = new StringBuilder();
      for (byte b : bytes) {
        sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
      }
      return sb.toString();
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
      throw new RuntimeException(ex);
    }
  }
}
