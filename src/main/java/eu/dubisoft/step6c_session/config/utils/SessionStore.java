package eu.dubisoft.step6c_session.config.utils;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SessionStore {

  public static void logServiceUsage(String serviceName) {
    SecurityUtil.currentSession().getExecutedServices().add(serviceName);
  }

  public static List<String> getExecutedServices() {
    return SecurityUtil.currentSession().getExecutedServices();
  }
}
