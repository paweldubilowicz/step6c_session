package eu.dubisoft.step6c_session.config.utils;

import eu.dubisoft.step6c_session.model.Session;
import java.util.ArrayList;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    public static Session currentSession() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            throw new IllegalStateException("No authorization available");
            // INFO: prawdopodobnie taki przypadek nie jest mozliwy, ale glowy nie dam
        }
        if (auth instanceof AnonymousAuthenticationToken) {
            // INFO: tutaj mozna wrzucic dodatkowa logike na zachowanie przy anonimowym dostepie
            // w przypadku anonimowego dostepu, w auth.getPrincipal() zawsze znajdzie sie string "anonymousUser"
            return new Session(null, null, null, new ArrayList<>(), true);
        }
        return (Session)auth.getPrincipal();
    }
}
