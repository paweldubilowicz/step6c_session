package eu.dubisoft.step6c_session;

import eu.dubisoft.step6c_session.config.utils.HashingUtils;
import eu.dubisoft.step6c_session.model.Permission;
import eu.dubisoft.step6c_session.model.User;
import eu.dubisoft.step6c_session.repository.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostConstructService {

  private final UserRepository userRepository;

  @PostConstruct
  private void init() {
    userRepository.addUser(new User("U1", "User1", HashingUtils.hashPassword("User1", "Password1"), Arrays.asList(Permission.ADMIN)));
    userRepository.addUser(new User("U2", "User2", HashingUtils.hashPassword("User2", "Password2"), Arrays.asList(Permission.USER)));
    userRepository.addUser(new User("U3", "User3", HashingUtils.hashPassword("User3", "Password3"), Arrays.asList(Permission.ADMIN, Permission.USER)));
    userRepository.addUser(new User("U4", "User4", HashingUtils.hashPassword("User4", "Password4"), Arrays.asList(Permission.FULL_ACCESS)));
    userRepository.addUser(new User("U5", "User5", HashingUtils.hashPassword("User5", "Password5"), new ArrayList<>()));
  }
}
