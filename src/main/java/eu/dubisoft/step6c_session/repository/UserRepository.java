package eu.dubisoft.step6c_session.repository;

import eu.dubisoft.step6c_session.model.User;

public interface UserRepository {

  User addUser(User user);

  User findByLoginAndPassword(String login, String password);
}
