package eu.dubisoft.step6c_session.repository;

import eu.dubisoft.step6c_session.model.Session;

public interface SessionRepository {

  void saveSession(Session session);

  Session findByToken(String token);

  void deleteByToken(String token);

  void removeAllExpiredSessions();

  boolean isTokenInUse(String token);
}
