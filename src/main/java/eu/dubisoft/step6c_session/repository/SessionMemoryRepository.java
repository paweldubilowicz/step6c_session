package eu.dubisoft.step6c_session.repository;

import eu.dubisoft.step6c_session.model.Session;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SessionMemoryRepository implements SessionRepository {

  // INFO: sesja moze byc przechowywana na wiele sposobow, jednak trzeba miec na uwadze, ze w procesie autoryzacji (czyli przy praktycznie kazdym requescie),
  // trzeba ja pobrac - warto miec ja w jakims zasobie podrecznym, np w pamieci, a nie w bazie. Niestety takie podejscie slabo dziala w mikroserwisach.
  // Ten przyklad pokazuje bezpieczne watkowo przechowywanie sesji w pamieci

  private final static Object lock = new Object();
  // INFO: pole SESSIONS przechowuje sesje, na ktorych mozna wykonywac modyfikacje, lecz tylko w blockach SYNCHRONIZED
  private final static Map<String, Session> SESSIONS = new TreeMap<>();
  // INFO: pole READ_ONLY_SESSIONS jest polem, do ktorego zapisywana jest odbitka sesji zawsze po modyfikacji,
  // a dzieki uzyciu AtomicReference pozwala na bezpieczny odczyt bez bloku synchronized
  private static AtomicReference<Map<String, Session>> READ_ONLY_SESSIONS = new AtomicReference<>(Collections.emptyMap());

  @Override
  public void saveSession(Session session) {
    synchronized(lock) {
      SESSIONS.put(session.getToken(), session);
      copySession();
    }
  }

  @Override
  public Session findByToken(String token) {
    return READ_ONLY_SESSIONS.get().get(token);
  }

  @Override
  public void deleteByToken(String token) {
    synchronized(lock) {
      SESSIONS.remove(token);
      copySession();
    }
  }

  @Override
  public void removeAllExpiredSessions() {
    synchronized(lock) {
      List<String> expiredTokens = SESSIONS.values().stream().filter(Session::isTokenExpired).map(Session::getToken).collect(Collectors.toList());
      for (String expiredToken : expiredTokens) {
        SESSIONS.remove(expiredToken);
      }
      // INFO: zostawiam loggery, zeby bylo widac, ze cos sie dzieje - na produkcji nie beda juz potrzebne
      if (expiredTokens.size() > 0) {
        if (expiredTokens.size() == 1) {
          log.info("Removed 1 expired session");
        } else {
          log.info("Removed " + expiredTokens.size() + " expired sessions");
        }
      }
      copySession();
    }
  }

  @Override
  public boolean isTokenInUse(String token) {
    return READ_ONLY_SESSIONS.get().containsKey(token);
  }

  private void copySession() {
    READ_ONLY_SESSIONS.set(new TreeMap<>(SESSIONS));
  }
}
