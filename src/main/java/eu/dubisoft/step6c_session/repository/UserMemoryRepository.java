package eu.dubisoft.step6c_session.repository;

import eu.dubisoft.step6c_session.config.utils.HashingUtils;
import eu.dubisoft.step6c_session.exceptions.InvalidCredentialsException;
import eu.dubisoft.step6c_session.model.User;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class UserMemoryRepository implements UserRepository {

  private final Map<String, User> USERS = new HashMap<>();

  @Override
  public User addUser(User user) {
    return USERS.put(user.getUserId(), user);
  }

  @Override
  public User findByLoginAndPassword(String username, String password) {
    String hashedPassword = HashingUtils.hashPassword(username, password);
    for (User u : USERS.values()) {
      if (u.getLogin().equals(username)) {
        if (!u.getPassword().equals(hashedPassword)) {
          throw new InvalidCredentialsException();
        }
        return u;
      }
    }
    throw new InvalidCredentialsException();
  }
}
