package eu.dubisoft.step6c_session.schedulers;

import eu.dubisoft.step6c_session.repository.SessionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ExpiredSessionCleanupScheduler {

  private final SessionRepository sessionRepository;

  // INFO: tutaj wstawilem constanta
  // Ladniej byloby miec to zdefiniowane jako property, ale jak wczesniej wspominalem, anotacje przyjmuja tylko constanty
  // Sa tez inne sposoby tworzenia springowych schedulerow, gdzie mozna uzyc zmiennych, ale to zostawiam juz do ogarniecia wam.
  @Scheduled(initialDelay = 60000, fixedRate = 30000)
  public void cleanup() {
    // INFO: zostawiam loggery, zeby bylo widac, ze cos sie dzieje - na produkcji nie beda juz potrzebne
    log.info("Cleaning up sessions");
    // INFO: co 30 sekund uruchomi sie proces sprawdzania, czy termin przyatnosci sesji juz minal, jezeli tak - wygasniete sesje zostaja usuniete z repozytorium
    sessionRepository.removeAllExpiredSessions();
  }
}
