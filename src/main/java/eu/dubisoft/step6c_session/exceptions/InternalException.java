package eu.dubisoft.step6c_session.exceptions;

public class InternalException extends RuntimeException {

  public InternalException() {
  }

  public InternalException(String message) {
    super(message);
  }
}
