No to doszliśmy do pierwszego przykładu, który ma pokazać, że Spring Security wcale nie jest taki super.
Prawdopodobnie coś bardzo podobnego jest dostępne out-of-the-box w Spring Security, ale... po co szukać, jak można zrobić po swojemu i to lepiej?

Endpointy z grubsza się pokrywają, dostępy też. W SecurityConfig.configure nie pojawia się nic nowego, więc o co chodzi?

Utworzyliśmy SessionRepository - w naszym przypadku jest to SessionMemoryRepository, które składuje informacje o sesji (na zasadzie podobnej do tego jak Spring robił to z JSESSIONID)
Dodaliśmy obsługę timeouta sesji, żeby jeszcze bardziej przypominało mechanizm z JSESSIONID, jednak musieliśmy samodzielnie to obsłużyć - dzięki temu możemy dosyć dowolnie wpływać na to, co się w momencie wygaśnięcia sesji wydarzy.
W przeciwieństwie do JWT - mamy opcję wylogowania się (czyli usunięcia sesji).
UWAGA: Rozwiązanie jest dalekie od ideału (taki przyklad mojego... eksperymentu z dzieciństwa), niesie za sobą poważne zagrożenie współbieżności, mianowicie:
Używamy 2 map dla zapewnienia bezpieczeństwa wątkowego, jednak obie mapy trzymają referencje do tego samego obiektu - czyli zmiana obiektu z jednej mapy jest widoczna w drugiej mapie.
Część mechanizmu (przechowywanie danych w obrębie sesji) wręcz na tym polega, ale zostawia to dużą podatność na błędy.
Przykładowo, w dowolnym miejscu w aplikacji możemy pobrać sesję z SecurityUtil i zmienić dowolną wartość wewnątrz niej - zmiana automatycznie propaguje się do obu map (tu też pobieramy tylko referencję).
Oczywiście istnieje sposób na zrobienie tego poprawnie, ale jego złożoność powoduje, że lepiej użyć na przykład pamięciowej bazy H2 (o tym w przykładzie step6e_custom).

Autentykacja wygląda podobnie - szukamy usera po loginie i haśle, jeżeli wszystko się zgadza to generujemy token, tworzymy sesję i zapisujemy ją w repo.
Token tworzymy prostym generatorem UUID, który zapewnia teoretyczną unikalność. Dodatkowo sprawdzamy czy wygenerowany token nie jest używany.
Jeżeli dla danego użytkownika sesja już istnieje - zostaje ona usunięta i utworzona od nowa (zaleznie od parametru w application.properties)
UWAGA: Można w przypadku tokenów typu UUID (zamiast sprawdzania istnienia tokena) weryfikować także coś więcej, na przykład username'a (przekazywać go wraz z tokenem w headerach) lub jakąś informację o maszynie, z której wyszedł request.
       Istnieje niezwykle mała szansa na to, że token UUID się powtórzy, nadpisze istniejącą sesję i nowo-zalogowany użytkownik automatycznie współdzieli swoją sesję z kimś innym (a kto inny ma po prostu dostęp do jego konta).

AuthorizationFilter - autoryzacja przebiega nieco inaczej, bardziej customowo.
Nadal zaczynamy od pobrania tokenu z headera. Dla odmiany przekazywany przez nagłówek "X_AUTH_TOKEN" - bo czemu nie? Czuję się też dzięki temu bezpieczniej, bo wiem, że Spring sam z siebie nic z tym nagłówkiem nie zacznie robić.
W przypadku braku tokena przerywamy autoryzację i puszczamy filtry dalej, jeżeli Spring uzna, że sesja nie jest wymagana przez endpoint - ruszamy dalej. Różni się to od przykładu z JWT tym, że tam wykluczaliśmy serwisy z filtracji, przez co dla otwartych serwisów (permitAll) nigdy nie moglibyśmy mieć sesji - tutaj jeżeli się da to sesję chcemy mieć zawsze.
Następnie używając tokena znajdujemy sesję i sprawdzamy czy nadal nie wygasła (czas życia definiowany w application.properties).
Jeżeli sesja jest nadal dobra, resetujemy jej termin przydatności (zaleznie od parametru w application.properties) i tak jak dotychczas osadzamy w SecurityContext.

W procesie autoryzacji minimalnie inaczej podszedłem do rzucania błędów - tam gdzie mogę, rzucam własne błędu, znowu przez paranoję, że Spring weźmie wyjątek i poczyni na nim automagie (jak na przykład z AccessDeniedException to robi).

Sesja jest trzymana w pamięci, więc restart aplikacji ją wyczyści. Repozytorium mozna zastąpić standardowym repozytorium bazodanowym, będzie to nieco bardziej persystentne podejście, ale jednak też bardziej wymagające.
Implementacja w przykładzie jest nieco bardziej złożona niż w przypadku repozytorium użytkowników (które jest tylko poglądowe), ale zapewnia bezpieczeństwo wątkowe.

Usuwanie sesji - tu pojawia się coś nowego - scheduling. Na początek potrzebna jest anotacja @EnableScheduling w mainie, albo w jakiejś klasie konfiguracyjnej.
Po drugie potrzebny sam scheduler - w naszym przypadku jest to /schedulers/ExpiredSessionCleanupScheduler
W nim definiujemy metody z anotacją określającą działanie schedulera (można też używać formatu cronowego) - zadania schedulowane odpalają się cyklicznie, zgodnie z definicją w anotacji.

Z nowości mamy też inne podejście do szyfrowania hasła - te standardowe Springowe passwordEncodery są niby spoko, ale daleko im do wysokiego standardu bezpieczeństwa.
Ja użyłem autorskiego podejścia zawartego w HashingUtils - szyfr jest wystarczająco bezpieczny, żeby używać go w warunkach produkcyjnych.
Ważne jest, żeby produkcyjny SALT nigdy nie znalazł się w repozytorium, ale również, żeby był niezmienny.

Jeszcze jedna ciekawostka (również dostępna poniekąd w Spring Security, ale tylko w przypadku używania springowej sesji) to przechowywanie danych wewnątrz sesji.
Nasz model sesji zawiera w tym przypadku dodatkowe pole List<String> executedServices , w do którego na bieżąco dorzucamy informacje o użytych endpointach.
Ta informacja propaguje się między wywołaniami w zakresie jednej sesji użytkownika. Są przypadki, kiedy taki mechanizm jest nam niezbędny, jednak warto w miarę możliwości zastąpić go czymś innym (np. pobieraniem danych bezpośrednio z bazy).
Dostęp do tego magazynu danych uzyskujemy poprzez statyczną klasę SessionStore.


Aplikacja zabezpieczona w niniejszy sposób jak najbardziej spełnia wymogi bezpieczeństwa w przypadku wielu klientów. Jednak można pójść dalej i zrobić coś więcej (o tym w przykładach step6d_oauth i step6e_custom)
